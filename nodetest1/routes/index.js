var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Index' });
});

/* User list page. */
router.get('/userlist', function(req, res, next){
	var db= req.db;
	var collection= db.get('usercollection');
	collection.find({},{}, function(e, docs){
		res.render('userlist', {'userlist': docs});
	});
});

/* User new page. */
router.get('/newuser', function(req, res, next){
	res.render('newuser', {'title': 'Add New User'});
});

/* User creation */
router.put('/adduser', function(req, res, next){

	// Set initial db variable
	var db= req.db;

	// Get values from the form
	var username= req.body.username;
	var email= req.body.useremail;

	// User collection object
	var usercollection= db.get('usercollection');

	// Submit to db
	usercollection.insert(
		{"username":  username,
		 "email": email
		}, function(err, doc){
			if(err)
				res.send("There was a problem trying to create the user");	
			else
				res.redirect("userlist");
		}
	);
});

module.exports = router;
