var express = require('express');
var router = express.Router();

// Get userlist
router.get('/userlist', function(req, res){
	var collection= req.db.get('userlist');
	collection.find({},{}, function(e, docs){
		res.json(docs);
	});
});

// Insert user
router.post('/adduser', function(req, res){
	var collection= req.db.get('userlist');
	collection.insert(req.body, function(err, result){
		res.send(
			(err == null) ? {msg: 'success'} : {msg: err}
		);
	});
});

// Delete user
router.delete('/deleteuser/:id', function(req, res){
	var collection= req.db.get('userlist');
	console.log(req.params.id);
	var userToDelete= req.params.id;
	collection.remove({ '_id': req.params.id }, function(err){

		res.send( err === null ? {msg:  'deleted'} : {msg: 'error: '+ error } )
	});
});

module.exports = router;
